import axios from "axios";
import qs from "qs";

const title = "คณะสถาปัตยกรรมศาสตร์และการผังเมือง - มหาวิทยาลัยธรรมศาสตร์";
const description =
  "สร้างทางเลือกในการศึกษาตามความถนัดของผู้เรียน ร่วมกับการสร้างองค์ความรู้ใหม่ของคณาจารย์ ผ่านการบริหารจัดการอย่างมีองค์รวม ผลิตบัณฑิตที่สามารถสร้างนวัตกรรมที่มีความรับผิดชอบต่อสังคมโลก และร่วมขับเคลื่อนมหาวิทยาลัยธรรมศาสตร์สู่สถาบันการศึกษาชั้นนำในระดับนานาชาติิ";
const gtag_id = "G-K7HGL7FEMQ";

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title,
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: description },
      { name: "format-detection", content: "telephone=no" },
      {
        hid: "og:title",
        property: "og:title",
        content: title,
      },
      {
        hid: "og:description",
        property: "og:description",
        content: description,
      },
      // {
      //   hid: "og:type",
      //   property: "og:type",
      //   content: "website",
      // },
      // {
      //   hid: "og:image",
      //   property: "og:image",
      //   content: og_image,
      // },
      // {
      //   hid: "og:url",
      //   property: "og:url",
      //   content: process.env.WEB_URL,
      // },
      // {
      //   hid: "twitter:title",
      //   name: "twitter:title",
      //   content: title,
      // },
      // {
      //   hid: "twitter:description",
      //   name: "twitter:description",
      //   content: description,
      // },
      // {
      //   hid: "twitter:card",
      //   name: "twitter:card",
      //   content: "summary_large_image",
      // },
      // {
      //   hid: "twitter:image",
      //   name: "twitter:image",
      //   content: og_image,
      // },
      // {
      //   hid: "twitter:url",
      //   property: "twitter:url",
      //   content: process.env.WEB_URL,
      // },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.jpg" },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined",
      },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css2?family=Archivo:wght@400;600;700&family=Prompt:wght@400&display=swap",
      },
    ],
    script: [
      {
        async: true,
        src: `https://www.googletagmanager.com/gtag/js?id=${gtag_id}`,
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["element-ui/lib/theme-chalk/index.css", "~/assets/style/index.scss"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: "~/plugins/app-api" },
    { src: "~/plugins/replace-media-path" },
    { src: "~/plugins/element-ui" },
    { src: "~/plugins/gtag", mode: "client" },
    { src: "~/plugins/cookie-consent", mode: "client" },
    { src: "~/plugins/carbon-icons", mode: "client" },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: ["@nuxtjs/style-resources"],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [["nuxt-mq"], "cookie-universal-nuxt"],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [/^element-ui/],
  },

  loading: {
    color: "#ff5500",
  },

  styleResources: {
    scss: ["~/assets/style/variables.scss"],
  },

  env: {
    api_url: process.env.API_URL,
    mapbox_token: process.env.MAPBOX_TOKEN,
  },

  target: "static",

  mq: {
    defaultBreakpoint: "desktop",
    breakpoints: {
      mobile: 1025,
      desktop: Infinity,
    },
  },

  publicRuntimeConfig: {
    gtag_id: gtag_id,
    title: title,
  },

  generate: {
    async routes() {
      const query = qs.stringify(
        { fields: ["createdAt"] },
        { encodeValuesOnly: true }
      );

      const program_query = qs.stringify(
        { fields: ["createdAt"] },
        { encodeValuesOnly: true }
      );

      const [event, announcement, exchange_program, other, research, program] =
        await Promise.allSettled([
          axios.get(`${process.env.API_URL}/api/events?${query}`),
          axios.get(`${process.env.API_URL}/api/announcements?${query}`),
          axios.get(`${process.env.API_URL}/api/exchange-programs?${query}`),
          axios.get(`${process.env.API_URL}/api/services?${query}`),
          axios.get(`${process.env.API_URL}/api/research-lists?${query}`),
          axios.get(`${process.env.API_URL}/api/programs?${program_query}`),
        ]);

      const event_data = event?.value?.data?.data?.map(
        (d) => `/events/${d.id}`
      );
      const announcement_data = announcement?.value?.data?.data?.map(
        (d) => `/announcements/${d.id}`
      );
      const exchange_program_data = exchange_program?.value?.data?.data?.map(
        (d) => `/exchange-program/${d.id}`
      );
      const other_data = other?.value?.data?.data?.map(
        (d) => `/others/${d.id}`
      );
      const research_data = research?.value?.data?.data?.map(
        (d) => `/research/${d.id}`
      );
      const program_data = program?.value?.data?.data?.map(
        (d) => `/programs/${d.id}`
      );

      return [
        ...(event_data ?? []),
        ...(announcement_data ?? []),
        ...(exchange_program_data ?? []),
        ...(other_data ?? []),
        ...(research_data ?? []),
        ...(program_data ?? []),
      ];
    },
  },
};
