import ArrowUpRight24 from "@carbon/icons-vue/es/arrow--up-right/24";
import ArrowLeft24 from "@carbon/icons-vue/es/arrow--left/24";
import Document24 from "@carbon/icons-vue/es/document/24";
import Download24 from "@carbon/icons-vue/es/download/24";
import Close24 from "@carbon/icons-vue/es/close/24";
import Close32 from "@carbon/icons-vue/es/close/32";
import Help32 from "@carbon/icons-vue/es/help/32";
import Vue from "vue";

Vue.component("ArrowUpRight24", ArrowUpRight24);
Vue.component("ArrowLeft24", ArrowLeft24);
Vue.component("Document24", Document24);
Vue.component("Download24", Download24);
Vue.component("Close24", Close24);
Vue.component("Close32", Close32);
Vue.component("Help32", Help32);
