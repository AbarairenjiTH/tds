import _ from "lodash";

export default function ({}, inject) {
  const replaceMediaPath = (value) => {
    return _.replace(
      value,
      /src="\/uploads/g,
      `src="${process.env.api_url}/uploads`
    );
  };

  inject("replaceMediaPath", replaceMediaPath);
}
