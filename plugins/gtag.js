export default function ({}, inject) {
  window.dataLayer = window.dataLayer || [];

  function gtag() {
    dataLayer.push(arguments);
  }

  inject("gtag", gtag);
}
