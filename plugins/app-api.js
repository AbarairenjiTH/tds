import axios from "axios";

export default function ({}, inject) {
  const appApi = axios.create({
    baseURL: `${process.env.api_url}/api`,
  });

  appApi.interceptors.response.use(
    function (response) {
      // Any status code that lie within the range of 2xx cause this function to trigger
      // Do something with response data
      return response?.data;
    },
    function (error) {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      // Do something with response error
      return Promise.reject(error);
    }
  );

  inject("api_url", process.env.api_url);
  inject("appApi", appApi);
}
